#ifdef __APPLE__

#include "common_defs.h"
#include "log.h"

#include <string>
#include <sched.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <cpuid.h>

#define CPUID(INFO, LEAF, SUBLEAF) __cpuid_count(LEAF, SUBLEAF, INFO[0], INFO[1], INFO[2], INFO[3])

#define GETCPU(CPU) {                              \
        uint32_t CPUInfo[4];                           \
        CPUID(CPUInfo, 1, 0);                          \
        /* CPUInfo[1] is EBX, bits 24-31 are APIC ID */ \
        if ( (CPUInfo[3] & (1 << 9)) == 0) {           \
          CPU = -1;  /* no APIC on chip */             \
        }                                              \
        else {                                         \
          CPU = (unsigned)CPUInfo[1] >> 24;                    \
        }                                              \
        if (CPU < 0) CPU = 0;                          \
      }


namespace OS {
    int getpid()
    {
        return ::getpid();
    }
    int getcid()
    {
        int cid;
        GETCPU(cid);
        return cid;
    }
    int64_t gettid()
    {
        uint64_t tid;
        pthread_threadid_np(NULL, &tid);
        return tid;
    }
	std::string rootPath()
    {
        return SOURCE_DIR;
	}
	void set_limits()
	{
		//pthread_setname_np(pthread_self(), "main.cpp");

		rlimit rl;
		if (getrlimit(RLIMIT_CORE, &rl) == -1)
		{
			Log::dbg("Getrlimit failed. This could be problem.\n");
		}
		else
		{
			rl.rlim_cur = rl.rlim_max;
			if (setrlimit(RLIMIT_CORE, &rl) == -1)
                Log::dbg("Setrlimit failed. May not save core.dump files.\n");
		}
	}
}

#endif
