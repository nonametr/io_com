#ifdef _WIN32

#include "os.h"

#include <algorithm>
#include <windows.h>
#include "engine_defs.h"

namespace OS {
    int getpid()
    {
        return GetCurrentProcessId();
    }
    int getcid()
    {
        return GetCurrentProcessorNumber();
    }
    int64_t gettid()
    {
        return GetCurrentThreadId();
    }
	std::string rootPath()
	{
        const uint32_t buffer_size = 1024;
        char buffer[buffer_size] = {'\0'};
        GetCurrentDirectory(buffer_size, buffer);

        std::string result(buffer);
        std::replace(result.begin(), result.end(), '\\', '/');
        return SOURCE_DIR;
	}
	void set_limits()
	{
	}
}

#endif
