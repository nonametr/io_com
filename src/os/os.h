#ifndef OS_H
#define OS_H

#include <stdint.h>
#include <string>

namespace OS {
	struct Helper{};
	void set_limits();
    int getpid();
    int getcid();
    int64_t gettid();
	std::string rootPath();    
	uint64_t readFile(const char* file_name, char* buffer, const uint32_t buffer_size);
	uint64_t writeFile(const char* file_name, const char* data, const uint64_t data_size);
}

#endif // OS_H
