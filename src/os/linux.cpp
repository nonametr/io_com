#ifdef __linux__

#include <string>
#include <sched.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/time.h>
#include <sys/resource.h>

namespace OS {
    int getpid()
    {
        return ::getpid();
    }
    int getcid()
    {
        return ::sched_getcpu();
    }
    int64_t gettid()
    {
        return ::syscall(SYS_gettid);
    }
	std::string rootPath()
    {
        return "";
	}
	void set_limits()
	{
		pthread_setname_np(pthread_self(), "main.cpp");

		rlimit rl;
		if (getrlimit(RLIMIT_CORE, &rl) == -1)
		{
            printf("Getrlimit failed. This could be problem.\n");
		}
		else
		{
			rl.rlim_cur = rl.rlim_max;
			if (setrlimit(RLIMIT_CORE, &rl) == -1)
                printf("Setrlimit failed. May not save core.dump files.\n");
		}
	}
}

#endif
