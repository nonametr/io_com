#include "os.h"
#include "log.h"

namespace OS {

uint64_t readFile(const char* file_name, char* buffer, const uint32_t buffer_size)
{
    std::FILE* fp = std::fopen(file_name, "r");
    ASSERT_WITH_CODE_NO_LOG(fp, return 0);

    std::fseek(fp, 0L, SEEK_END);
    const int file_size = ftell(fp);
    std::fseek(fp, 0L, SEEK_SET);

    uint64_t result = std::fread(buffer, sizeof(char), file_size, fp);

    std::fclose(fp);
    return result;
}

uint64_t writeFile(const char* file_name, const char* data, const uint64_t data_size)
{
    std::FILE* fp = std::fopen(file_name, "wt");

    uint64_t result = std::fwrite(data, sizeof(char), data_size, fp);
    std::fclose(fp);
    return result;
}

}
