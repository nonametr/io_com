#include "serial_nix.h"
#include "log.h"

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <map>

#ifdef unix
void NixSerial::open(std::string device)
{
    fd = ::open(device.c_str(), O_RDWR | O_NOCTTY | O_SYNC);
    if(fd < 0)
    {
        printf("Error opening %s: %s\n", device.c_str(), strerror(errno));
        exit(EXIT_FAILURE);
    }
}

void NixSerial::close()
{
    ::close(fd);
}

void NixSerial::configure(const DeviceConfig& config)
{
    static const std::map<uint32_t, speed_t> crb_map = {
        { 50, B50 },
        { 110, B110 },
        { 200, B200 },
        { 300, B300 },
        { 600, B600 },
        { 1200, B1200 },
        { 1800, B1800 },
        { 2400, B2400 },
        { 4800, B4800 },
        { 9600, B9600 },
        { 19200, B19200 },
        { 38400, B38400 },
    };
    static const std::map<uint32_t, uint32_t> byte_size_map = {
        { 5, CS5 },
        { 6, CS6 },
        { 7, CS7 },
        { 8, CS8 }
    };

    auto it_crb = crb_map.find(config.cbr);
    auto it_byte_size = byte_size_map.find(config.byte_size);

    ASSERT_WITH_CODE(it_crb != crb_map.end(), exit(EXIT_FAILURE), "Unsuported CRB");
    ASSERT_WITH_CODE(it_byte_size != byte_size_map.end(), exit(EXIT_FAILURE), "Unsuported byte size");

    speed_t cbr = it_crb->second;
    uint byte_size = it_byte_size->second;

    struct termios tty;

    ASSERT_WITH_CODE(tcgetattr (fd, &tty) == 0, exit(EXIT_FAILURE), "error %d from tcgetattr", errno);

    cfsetospeed (&tty, cbr);
    cfsetispeed (&tty, cbr);

    tty.c_cflag |= byte_size;                        // char size
    tty.c_iflag &= ~IGNBRK;                         // disable break processing
    tty.c_lflag = 0;                                // no signaling chars, no echo,
                                                    // no canonical processing
    tty.c_oflag = 0;                                // no remapping, no delays
    tty.c_cc[VMIN]  = 0;                            // read doesn't block
    tty.c_cc[VTIME] = config.timeout;               // read timeout

    tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl
    tty.c_cflag |= (CLOCAL | CREAD);        // ignore modem controls,
                                            // enable reading
    switch(config.parity)
    {
    case 0:
        tty.c_cflag &= ~PARENB;
        tty.c_cflag &= ~PARODD;
        break;
    case 1:
        tty.c_cflag |= PARENB;
        tty.c_cflag |= PARODD;
        break;
    case 2:
        tty.c_cflag |= PARENB;
        tty.c_cflag &= ~PARODD;
        break;
    }

    if(config.stop_bits == 1)
    {

        tty.c_cflag &= ~CSTOPB;
    }
    else
    {
        tty.c_cflag |= CSTOPB;
    }

    tty.c_cflag &= ~CRTSCTS;

    ASSERT_WITH_CODE (tcsetattr (fd, TCSANOW, &tty) == 0, exit(EXIT_FAILURE), "error %d from tcsetattr", errno);
}

std::vector<uint8_t> NixSerial::read()
{
    std::vector<uint8_t> result;
    do {
        //char tmp[128] = {'\0'};
        char ch;
        int rdlen;

        //rdlen = ::read(fd, tmp, sizeof(tmp));
        rdlen = ::read(fd, &ch, sizeof(ch));
        if (rdlen > 0)
        {
            //for(uint8_t ch : result)
                result.push_back(ch);
        }
        else if (rdlen < 0)
        {
            printf("Error from read: %d: %s\n", rdlen, strerror(errno));
            break;
        }
        else
        {
            if(result.empty())
            {
                printf("Timeout from read\n");
            }
            break;
        }
    } while (true);
    return result;
}

void NixSerial::write(const std::vector<uint8_t> &bytes)
{
    uint32_t write_size = ::write(fd, bytes.data(), bytes.size());
    if (write_size != bytes.size())
    {
        printf("Warning written(%d) != send(%zu), error: %d\n", write_size, bytes.size(), errno);
    }
    tcdrain(fd);
}
#endif
