﻿#include "serial_windows.h"

#ifdef WIN32
#include <windows.h>

void WinSerial::open(std::string device)
{
    HANDLE hComm;

    hComm = CreateFile(device.c_str(),                //port name
        GENERIC_READ | GENERIC_WRITE, //Read/Write
        0,                            // No Sharing
        NULL,                         // No Security
        OPEN_EXISTING,// Open existing port only
        0,            // Non Overlapped I/O
        NULL);        // Null for Comm Devices

    if (hComm == INVALID_HANDLE_VALUE)
    {
        printf("Error opening serial port\n");
        exit(EXIT_FAILURE);
    }
    else
        printf("Serial port opened successfully\n");
}

void WinSerial::close()
{

}
void WinSerial::configure(const DeviceConfig& config)
{

}
std::vector<uint8_t> WinSerial::read()
{
    std::vector<uint8_t> result;
    return result;
}
void WinSerial::write(const std::vector<uint8_t>& bytes)
{

}
#endif
/*

#include "lyra/help.hpp"
#include "lyra/arg.hpp"
#include "lyra/opt.hpp"
#include "lyra/cli_parser.hpp"
#include "lyra/exe_name.hpp"

#include <vector>
#include <string>
#include <sstream>

using namespace std;

struct CLI_Params
{
    int port_number = 1;
    int cbr = CBR_9600;
    int byte_size = 8;
    int stop_bits = ONESTOPBIT;
    int parity = NOPARITY;
    int timeout = 10;
    int output_format = 0;
    std::string write_data;
    std::string output_file = "raw.out";
    bool show_help = false;
};

const char* byte_to_binary(int x)
{
    static char b[9];
    b[0] = '\0';

    int z;
    for (z = 128; z > 0; z >>= 1)
    {
        strcat(b, ((x & z) == z) ? "1" : "0");
    }

    return b;
}

int main(int argc, char* argv[])
{
    CLI_Params settings;

    auto cli = lyra::help(settings.show_help)
                | lyra::opt(settings.port_number, "serial port number")["-p"]["--port"]("port number [default => 1]")
                | lyra::opt(settings.write_data, "data to write")["-w"]["--write"]("Hex encoded byte sequence to write [default => '']")
                | lyra::opt(settings.output_file, "file to put received data in")["-o"]["--output"]("File in current(working) dirrectory to write received data in [default => 'raw.out']")
                | lyra::opt(settings.output_format, "format of output file")["-f"]["--output_format"]("Format of output file [0 => raw(default) 2 => BIN, 10 => DEC, 8 => OCT, 16 => HEX]")
                | lyra::opt(settings.cbr, "baud rates at which the communication device operates")["-c"]["--cbr"]("port cbr [default => 9600]")
                | lyra::opt(settings.byte_size, "byte size")["-b"]["--byte_size"]("Byte Size. [default => 8]")
                | lyra::opt(settings.timeout, "byte size")["-t"]["--timeout"]("Timeout. [default => 10]")
                | lyra::opt(settings.stop_bits, "stop bits")["-s"]["--stop_bits"]("Stop bits count. [default => 1]");

    auto cli_result = cli.parse({ argc, argv });
    if (!cli_result)
    {
        printf("Error in command line: %s", cli_result.errorMessage().c_str());
        std::exit(EXIT_FAILURE);
    }

    if (settings.show_help)
    {
        std::cout << cli << "\n";
        exit(EXIT_SUCCESS);
    }

    HANDLE hComm;

    hComm = CreateFile(("\\\\.\\COM" + std::to_string(settings.port_number)).c_str(),                //port name
        GENERIC_READ | GENERIC_WRITE, //Read/Write
        0,                            // No Sharing
        NULL,                         // No Security
        OPEN_EXISTING,// Open existing port only
        0,            // Non Overlapped I/O
        NULL);        // Null for Comm Devices

    if (hComm == INVALID_HANDLE_VALUE)
    {
        printf("Error opening serial port\n");
        exit(EXIT_FAILURE);
    }
    else
        printf("Serial port opened successfully\n");

    DCB dcbSerialParams = { 0 }; // Initializing DCB structure
    dcbSerialParams.DCBlength = sizeof(dcbSerialParams);

    bool Status = GetCommState(hComm, &dcbSerialParams);
    dcbSerialParams.BaudRate = settings.cbr;  // Setting BaudRate = 9600
    dcbSerialParams.ByteSize = settings.byte_size;         // Setting ByteSize = 8
    dcbSerialParams.StopBits = settings.stop_bits;// Setting StopBits = 1
    dcbSerialParams.Parity = settings.parity;  // Setting Parity = None

    SetCommState(hComm, &dcbSerialParams);

    COMMTIMEOUTS timeouts = { 0 };
    timeouts.ReadIntervalTimeout = settings.timeout * 5; // in milliseconds
    timeouts.ReadTotalTimeoutConstant = settings.timeout * 5; // in milliseconds
    timeouts.ReadTotalTimeoutMultiplier = settings.timeout; // in milliseconds
    timeouts.WriteTotalTimeoutConstant = settings.timeout * 5; // in milliseconds
    timeouts.WriteTotalTimeoutMultiplier = settings.timeout; // in milliseconds

    SetCommTimeouts(hComm, &timeouts);

    DWORD dNoOfBytesWritten = 0;     // No of bytes written to the port

    //char input[] = "0x0x3";
    char* input = settings.write_data.data();
    char* token = strtok(input, "x");

    vector<std::string> hex_tokens;
    while (token != NULL)
    {
        hex_tokens.push_back(token);
        token = strtok(NULL, "x");
    }

    uint8_t* byte_array = (uint8_t*) malloc(sizeof(uint8_t) * hex_tokens.size());
    memset(byte_array, 0, sizeof(uint8_t) * hex_tokens.size());
    for(int i = 0; i < hex_tokens.size(); ++i)
    {
        byte_array[i] = std::stoi(hex_tokens[i], 0, 16);
        printf("sending byte #%u => %s\n", i, byte_to_binary(byte_array[i]));
    }

    Status = WriteFile(hComm,        // Handle to the Serial port
        byte_array,     // Data to be written to the port
        sizeof(uint8_t) * hex_tokens.size(),  //No of bytes to write
        &dNoOfBytesWritten, //Bytes written
        NULL);

    printf("#%u count bytes send!\n", dNoOfBytesWritten);
    printf("Waiting for response...\n");

    char TempChar = 0; //Temporary character used for reading
    uint8_t SerialBuffer[256] = { '\0' };//Buffer for storing Rxed Data
    DWORD NoBytesRead;
    int bytes_read = 0;

    do
    {
        ReadFile(hComm,					//Handle of the Serial port
            &TempChar,       //Temporary character
            sizeof(TempChar),//Size of TempChar
            &NoBytesRead,    //Number of bytes read
            NULL);

        if (NoBytesRead <= 0 || bytes_read >= 254)
        {
            if (bytes_read == 0)
            {
                printf("Read op. timed out. Terminating...\n");
            }
            break;
        }

        SerialBuffer[bytes_read] = TempChar;// Store Tempchar into buffer
        bytes_read++;
    }
    while (true);

    printf("Received => %u byte(s), output written to file ./raw.out\n", bytes_read);

    std::FILE* fp = std::fopen(settings.output_file.c_str(), "wb");

    if (settings.output_format != 0)
    {
        std::stringstream result_stream;
        switch (settings.output_format)
        {
        case 2:
            for (int i = 0; i < bytes_read; ++i)
            {
                result_stream << byte_to_binary(SerialBuffer[i]);
            }
            break;
        case 8:
            for (int i = 0; i < bytes_read; ++i)
            {
                result_stream << 'x' << std::oct << (int)SerialBuffer[i];
            }
            break;
        case 16:
            for (int i = 0; i < bytes_read; ++i)
            {
                result_stream << 'x' << std::hex << (int)SerialBuffer[i];
            }
            break;
        default:
            printf("Unsupported output format\n");
        case 10:
            for (int i = 0; i < bytes_read; ++i)
            {
                result_stream << SerialBuffer[i];
            }
            break;
        }

        std::string result = result_stream.str();

        uint64_t f_result = std::fwrite(result.c_str(), sizeof(char), result.length(), fp);
    }
    else
    {
        uint64_t f_result = std::fwrite(SerialBuffer, sizeof(char), bytes_read, fp);
    }
    std::fclose(fp);
    std::fflush(fp);
    CloseHandle(hComm);//Closing the Serial Port

    return EXIT_SUCCESS;
}
*/
