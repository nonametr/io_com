#pragma once

#include <string>
#include <vector>

struct DeviceConfig
{
    int cbr = 4800;
    int byte_size = 8;
    int stop_bits = 1;
    int parity = 2;
    int timeout = 10;
};

struct IsSerial
{
    virtual void open(std::string device) = 0;
    virtual void close() = 0;
    virtual void configure(const DeviceConfig& config) = 0;
    virtual std::vector<uint8_t> read() = 0;
    virtual void write(const std::vector<uint8_t>& bytes) = 0;
};

namespace Serial
{
    IsSerial* create();
}
