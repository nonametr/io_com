#include <iostream>
#include <cstring>

#include <lyra/lyra.hpp>


#include "is_serial.h"

struct CLI_Params : public DeviceConfig
{
    std::string port;
    int timeout = 10;
    int output_format = 16;
    std::string write_data;
    std::string output_file = "";
    bool silent = false;
    bool show_help = false;
    bool dbg_write_bytes = true;
};

const char* byte_to_binary(int x)
{
    static char b[9];
    b[0] = '\0';

    int z;
    for (z = 128; z > 0; z >>= 1)
    {
        strcat(b, ((x & z) == z) ? "1" : "0");
    }

    return b;
}

std::vector<uint8_t> tokenize(std::string& str)
{
    std::vector<uint8_t> result;

    char* input = str.data();
    char* token = strtok(input, "x");

    std::vector<std::string> hex_tokens;
    while (token != NULL)
    {
        hex_tokens.push_back(token);
        token = strtok(NULL, "x");
    }

    for(const std::string &token : hex_tokens)
    {
        result.push_back(std::stoi(token, 0, 16));
    }

    return result;
}

std::string formatOutput(std::vector<uint8_t> bytes, int format)
{
    std::stringstream result_stream;
    switch (format)
    {
    case 2:
        for (uint32_t i = 0; i < bytes.size(); ++i)
        {
            result_stream << byte_to_binary(bytes[i]);
        }
        break;
    case 8:
        for (uint32_t i = 0; i < bytes.size(); ++i)
        {
            result_stream << 'x' << std::oct << (int)bytes[i];
        }
        break;
    case 16:
        for (uint32_t i = 0; i < bytes.size(); ++i)
        {
            result_stream << 'x' << std::hex << (int)bytes[i];
        }
        break;
    case 10:
        for (uint32_t i = 0; i < bytes.size(); ++i)
        {
            result_stream << std::hex << (int)bytes[i];
        }
        break;        
    default:
        printf("Unsupported output format\n");
        break;
    }

    return result_stream.str();
}

int main(int argc, char* argv[])
{
    CLI_Params settings;

    auto cli = lyra::help(settings.show_help)
                | lyra::opt(settings.port, "serial port name")["-p"]["--port"]("port name").required()
                | lyra::opt(settings.write_data, "data to write")["-w"]["--write"]("Hex encoded byte sequence to write [default => '']")
                | lyra::opt(settings.output_file, "file to put received data in")["-o"]["--output"]("File in current(working) dirrectory to write received data in [default => 'not write into file']")
                | lyra::opt(settings.output_format, "format of output file")["-f"]["--output_format"]("Format of output file [2 => BIN, 10 => DEC, 8 => OCT, 16 => HEX]").required()
                | lyra::opt(settings.cbr, "baud rates at which the communication device operates")["-c"]["--cbr"]("port cbr [default => 9600]")
                | lyra::opt(settings.byte_size, "byte size")["-b"]["--byte_size"]("Byte Size. [default => 8]")
                | lyra::opt(settings.timeout, "timeout for read and write operations in ms")["-t"]["--timeout"]("Timeout. [default => 10]")
                | lyra::opt(settings.parity, "parity control")["--parity"]("Parity. [0(default) => disable, 1 => ODD, 2 => EVEN]")
                | lyra::opt(settings.silent)["--silent"]("print only result, no debug or info messages")
                | lyra::opt(settings.stop_bits, "stop bits")["-s"]["--stop_bits"]("Stop bits count. [default => 1]");

    auto cli_result = cli.parse({ argc, argv });
    if (!cli_result)
    {
        printf("Error in command line: %s", cli_result.errorMessage().c_str());
        std::exit(EXIT_FAILURE);
    }

    if (settings.show_help)
    {
        std::cout << cli << "\n";
        exit(EXIT_SUCCESS);
    }

    if(settings.write_data.empty())
    {
        printf("Missing required command line arguments [type -h for help]\n");
        exit(EXIT_SUCCESS);
    }

    std::vector<uint8_t> bytes = tokenize(settings.write_data);

    if(!settings.silent)
    {
        for(uint32_t i = 0; i < bytes.size(); ++i)
        {
            printf("sending byte #%u => %s\n", i, byte_to_binary(bytes[i]));
        }
    }

    IsSerial* serial = Serial::create();
    serial->open(settings.port.c_str());
    serial->configure(settings);
    serial->write(bytes);
    std::vector<uint8_t> bytes_read = serial->read();
    serial->close();

    std::string result = formatOutput(bytes_read, settings.output_format);

    if(!settings.output_file.empty())
    {
        std::FILE* fp = std::fopen(settings.output_file.c_str(), "wb");
        std::fwrite(bytes.data(), sizeof(uint8_t), bytes.size(), fp);
        std::fflush(fp);
        std::fclose(fp);
    }
    else
    {
        printf("{\"result\": \"%s\"}\n", result.c_str());
    }

    return EXIT_SUCCESS;
}
