#include "is_serial.h"

#ifdef unix
#include "serial_nix.h"
#else
#include "serial_windows.h"
#endif

IsSerial* Serial::create()
{
#ifdef unix
    return new NixSerial();
#else
    return new WinSerial();
#endif

}
