#ifndef LOG_H
#define LOG_H

#define ASSERT_WITH_CODE_NO_LOG(cond, code) \
if (!(cond))                            \
{                                       \
    code;                               \
}[](){}()

#define ASSERT_WITH_CODE(cond, code, ...) \
if (!(cond))                          \
{                                     \
    printf(__VA_ARGS__);            \
    code;                             \
}[](){}()

#define ASSERT_LOG(cond, ...)  \
if (!(cond))               \
{                          \
    printf(__VA_ARGS__); \
}[](){}()

#endif // LOG_H
