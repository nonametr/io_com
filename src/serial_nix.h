#pragma once

#ifdef unix

#include <iostream>

#include "is_serial.h"

struct NixSerial final : public IsSerial
{
    virtual void open(std::string device) override;
    virtual void close() override;
    virtual void configure(const DeviceConfig& config) override;
    virtual std::vector<uint8_t> read() override;
    virtual void write(const std::vector<uint8_t> &bytes) override;

private:
    int fd;
};

#endif
