﻿#pragma once

#ifdef WIN32
#include "is_serial.h"

struct WinSerial final : public IsSerial
{
    virtual void open(std::string device) override;
    virtual void close() override;
    virtual void configure(const DeviceConfig& config) override;
    virtual std::vector<uint8_t> read() override;
    virtual void write(const std::vector<uint8_t>& bytes) override;
};
#endif
