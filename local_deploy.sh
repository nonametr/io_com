#!/bin/bash

target_ip="192.168.255.10"

sudo docker run --rm -it -v $(pwd):/app rikudousage/qt-static:5.13.1 -c "cd /app && mkdir -p build && cd build && make clean && cmake -DCMAKE_BUILD_TYPE=Release .. && make VERBOSE=1 -j"
scp ./build/io_com root@$target_ip:/home/otlex/KioskDE/
